import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Auth } from './auth';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  auth: Auth;

  constructor(private http: HttpClient) { }

  signin(username: string, password: string) {
    return new Promise((resolve, reject) => {
      this.http.get<Auth>('http://localhost:3000/login')
        .subscribe(res => {
          this.auth = res;
          localStorage.setItem('token', res.token)
          resolve(res);
        });
    });
  }

  logout() {
    localStorage.removeItem('token');
  }
  isLogged(): boolean {
    return !!localStorage.getItem('token')
    // return this.auth && !!this.auth.token;
  }

  getToken() {
    return localStorage.getItem('token');
  }
}
