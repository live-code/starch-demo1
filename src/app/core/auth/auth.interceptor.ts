import { Injectable } from '@angular/core';
import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { AuthService } from './auth.service';
import { catchError, filter, map } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthInterceptor implements HttpInterceptor {
  constructor(private authService: AuthService, private router: Router) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let newReq = req;

    if (this.authService.isLogged()) {
      newReq = req.clone({
        setHeaders: { Authorization: this.authService.getToken()}
      });
    }

    return next.handle(newReq)
      .pipe(
        catchError((err) => {
          console.log('interceptor')
          if (err instanceof HttpErrorResponse) {
            switch (err.status) {
              case 0:
              case 401:
              default:
                this.router.navigateByUrl('');
            }
          }

          return throwError(err);
          // return of(null);
        })
      );
  }

}
