import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { ThemeService } from '../settings/theme.service';
import { AuthService } from '../auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  template: `
    <div 
      style="padding: 10px"
      [style.background-color]="themeService.value === 'dark' ? '#222' : '#ccc'"
    >
      <button 
        routerLinkActive="actived" 
        [routerLinkActiveOptions]="{ exact: true}" 
        [routerLink]="''"
        *ngIf="!authService.isLogged()"
      >login</button>
      
      <button routerLinkActive="actived" [routerLink]="'home'">home</button>
      
      <button
        *ngIf="authService.isLogged()"
        routerLinkActive="actived" 
        routerLink="catalog">catalog</button>
      
      <button
        routerLinkActive="actived" 
        routerLink="settings">settings</button>
      
      <button routerLinkActive="actived" routerLink="contacts">contacts</button>


      <button
        *ngIf="authService.isLogged()"
        (click)="logoutHandler()"
        >Logout</button>


    </div>
    
    <pre>{{themeService.value}}</pre>
    
    <hr>
  `,
  styles: [`
    .actived {
      background-color: red;
    }
  `]
})
export class NavbarComponent implements OnInit {

  constructor(
    public themeService: ThemeService,
    public authService: AuthService,
    private router: Router
  ) {}

  ngOnInit(): void {
  }

  logoutHandler() {
    this.authService.logout();
    this.router.navigateByUrl('')

  }
}
