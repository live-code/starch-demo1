import { Injectable } from '@angular/core';
import { CatalogService } from '../../features/catalog/services/catalog.service';

@Injectable({
  providedIn: 'root'
})
export class ThemeService {
  value = 'dark';

  setTheme(value: string) {
    this.value = value;
  }

}
