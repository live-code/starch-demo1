import { Component, OnInit } from '@angular/core';
import { ThemeService } from '../../core/settings/theme.service';

@Component({
  selector: 'app-settings',
  template: `
    L'attuale tema è {{themeService.value}}
    <button (click)="themeService.setTheme('light')">
      light
    </button>

    <button (click)="themeService.setTheme('dark')">
      dark
    </button>
  `,

})
export class SettingsComponent {
  constructor(public themeService: ThemeService) {
  }

  toggle() {
    console.log(this.themeService.value)
  }

}
