import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../core/auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  template: `


    <div style="max-width: 400px; margin: 0 auto">

      <app-card title="SIGNIN" theme="dark">
        <form #f="ngForm" (submit)="login(f.value)">
          <input class="form-control" 
                 type="text" [ngModel] name="username">
          <input class="form-control" 
                 type="password" [ngModel] name="password">
          <button type="submit" class="btn btn-outline-primary btn-block">LOGIN</button>
        </form>
      </app-card>
    </div>


    <app-jumbotron title="ciao">
      FB Training
    </app-jumbotron>
    
    <!--<app-row>
      <app-col>A</app-col>
      <app-col><button>B</button></app-col>
      <app-col>C</app-col>
    </app-row>-->    
    
  `,
})
export class LoginComponent {
  constructor(
    public authService: AuthService,
    private router: Router
  ) {
  }


  login(formData: { username: string, password: string}) {
    const { username, password } = formData;
    this.authService.signin(username, password)
      .then(res => {
        this.router.navigateByUrl('home')
      });
  }
}

