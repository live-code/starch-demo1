import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  template: `
    <app-card
      icon="fa fa-linkedin"
      url="http://www.linkedin.com"
      title="Profile"
      [theme]="myTheme"
      (iconClick)="openUrl('http://www.linkedin.com')"
    >
      <input>
      <input>
      <input>
    </app-card>

    <app-card 
      title="Facebook"
      url="http://www.facebook.com"
      icon="fa fa-facebook"
      (iconClick)="openUrl('http://www.google.com')"
    >
      <app-hello></app-hello>
    </app-card>
    
    
    <app-card title="nested" icon="fa fa-list" (iconClick)="log()">
      <div class="row">
        <div class="col">
          <app-card title="left">A</app-card>
        </div>

        <div class="col">
          <app-card title="right">B</app-card>
        </div>
      </div>
      
    </app-card>

    <button (click)="toggle('dark')">dark</button>
    <button (click)="toggle('light')">light</button>
  
    <hr>
    
    <app-tabbar 
      [items]="cars"
      [active]="activeCar"
      (tabClick)="selectCarHandler($event)"
    ></app-tabbar>

    <app-tabbar
      [items]="activeCar.models"
      [active]="activeModel"
      (tabClick)="selectModelHandler($event)"
      labelField="label"
    ></app-tabbar>
    
    <app-card [title]="activeModel.label">
      {{activeModel.desc}}
    </app-card>
  `
})
export class HomeComponent {
  myTheme = 'dark';

  cars = [
    {
      id: 1,
      name: 'Fiat',
      url: 'http://www.xyz1.com',
      models: [
        { id: 10, label: 'Panda', desc: 'bla bla 1' },
        { id: 11, label: 'Tipo', desc: 'bla bla 2' },
        { id: 12, label: 'Multipla', desc: 'bla bla 2' }
      ]
    },
    {
      id: 2,
      name: 'Merz',
      url: 'http://www.xyz2.com',
      models: [
        { id: 20, label: 'CLK', desc: 'germania bla bla 1' },
        { id: 21, label: 'SLK', desc: 'tedeschi bla bla 2' },
      ]
    },
    {
      id: 3,
      name: 'Alfa',
      url: 'http://www.xyz3.com',
      models: [
        { id: 30, label: 'Giulia', desc: '1 212bla bla 1' },
        { id: 31, label: 'Romeo', desc: 'wegwegew bla bla 2' },
        { id: 32, label: 'Giulietta', desc: ' ewtewbla bla 2' },
        { id: 33, label: 'Ciccio', desc: ' wetwe bla bla 2' }
      ]
    }
  ];

  activeCar;
  activeModel;

  constructor() {
    this.selectCarHandler(this.cars[0])
  }

  selectCarHandler(item) {
    this.activeCar = item;
    this.activeModel = item.models[0]
  }

  selectModelHandler(item) {
    this.activeModel = item;
  }

  openUrl(url: string) {
    window.open(url)
  }

  toggle(value: string) {
    this.myTheme = value;
  }

  log() {
    console.log('ciao')
  }

}
