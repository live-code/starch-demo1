import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-catalog-list-item-price',
  template: `
    <span [style.color]="value > 900 ? 'red' : null">€ {{value}}</span>
  `,
})
export class CatalogListItemPriceComponent {
  @Input() value: number

}
