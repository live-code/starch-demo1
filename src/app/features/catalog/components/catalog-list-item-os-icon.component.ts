import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-catalog-list-item-os-icon',
  template: `
    <i
      class="fa"
      [ngClass]="{
          'fa-apple': os === 'ios',
          'fa-android': os === 'android'
        }"
      [style.color]="os === 'ios' ? 'grey' : 'lightgreen'"
    ></i>
  `,
})
export class CatalogListItemOsIconComponent {
  @Input() os: 'ios' | 'android';
}
