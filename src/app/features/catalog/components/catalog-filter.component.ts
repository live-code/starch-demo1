import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-catalog-filter',
  template: `
    <input type="text" #input (input)="filterByName.emit(input.value)">
  `,
})
export class CatalogFilterComponent {
  @Output() filterByName: EventEmitter<string> = new EventEmitter<string>()
}
