import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { Device } from '../../../model/device';

@Component({
  selector: 'app-catalog-list',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <app-catalog-list-item
      *ngFor="let device of (devices | filterByName: text)"
      [device]="device"
      [selected]="device.id === active?.id"
      (setActive)="setActive.emit($event)"
      (delete)="delete.emit($event)"
      (gotoDetails)="gotoDetails.emit($event)"
    > </app-catalog-list-item>
  `,
})
export class CatalogListComponent  {
  @Input() devices: Device[];
  @Input() active: Device;
  @Input() text: string;
  @Output() setActive: EventEmitter<Device> = new EventEmitter<Device>();
  @Output() delete: EventEmitter<number> = new EventEmitter<number>();
  @Output() gotoDetails: EventEmitter<number> = new EventEmitter<number>();

}

