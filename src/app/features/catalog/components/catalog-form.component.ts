import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  Output,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import { Device } from '../../../model/device';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-catalog-form',
  // changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <div *ngIf="f.invalid && f.dirty" class="alert alert-danger">
      Form invalid
    </div>

    <form #f="ngForm" (submit)="save.emit(f.value)">
      <input
        type="text"
        placeholder="name"
        [ngModel]="active?.name"
        #inputName="ngModel"
        name="name"
        required
        minlength="3"
        class="form-control"
        [ngClass]="{
          'is-invalid': inputName.invalid && f.dirty,
          'is-valid': inputName.valid
        }"
      >
 

      <input
        type="text"
        placeholder="price"
        [ngModel]="active?.price"
        #inputPrice="ngModel"
        name="price"
        required
        class="form-control is-valid"
        [ngClass]="{
          'is-invalid': inputPrice.invalid && f.dirty,
          'is-valid': inputPrice.valid
        }"
      >
      <br>
      
      <pre>{{active?.os | json}}</pre>
      <select 
        class="form-control"
        [ngModel]="active?.os" 
        name="os"
        required
      >
        <option [ngValue]="null">Select os</option>
        <option value="android">Android</option>
        <option value="ios">Apple</option>
      </select>
      
      
      <button type="submit" [disabled]="f.invalid" class="btn btn-outline-primary">
        {{active?.id ? 'EDIT' : 'ADD'}}
      </button>

      <button (click)="cleanHandler()" type="button">CLEAN</button>
    </form>
  `,
})
export class CatalogFormComponent implements OnChanges, OnDestroy {
  @ViewChild('f', { static: true }) form: NgForm;
  @Input() active: Device;
  @Input() titleName: string;
  @Output() save: EventEmitter<Device> = new EventEmitter();
  @Output() clean: EventEmitter<void> = new EventEmitter();

  ngOnChanges(changes: SimpleChanges) {
    if (changes.active && !this.active.id) {
      this.form.reset();
    }
  }
  render() {
    console.log('render')
  }
  cleanHandler() {
    this.clean.emit();
    this.resetForm();
  }

  resetForm() {
    this.form.reset();
  }

  ngOnDestroy() {
    console.log('destroy')
  }
}
