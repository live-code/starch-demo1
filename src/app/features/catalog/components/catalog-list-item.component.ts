import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Device } from '../../../model/device';

@Component({
  selector: 'app-catalog-list-item',
  template: `
    <li
      class="list-group-item"
      (click)="setActive.emit(device)"
      [ngClass]="{'active' : selected}"
    >
      <app-catalog-list-item-os-icon [os]="device.os"></app-catalog-list-item-os-icon>
 
      <span>{{device.name}}</span>
       
      <app-catalog-list-item-price [value]="device.price"></app-catalog-list-item-price>

      <div class="pull-right">
        <i class="fa fa-arrow-down" (click)="toggle($event)"></i>
        <i 
          class="fa fa-info-circle"
          (click)="gotoDetailsHandler($event, device.id)"
        ></i>

        <!-- [routerLink]="'/catalog/details/' + device.id"-->

        <i class="fa fa-trash"
           (click)="deleteHandler($event, device.id)"></i>
      </div>

      <div *ngIf="!opened">
        descrizioneprodotto
      </div>
    </li>
  `,
})
export class CatalogListItemComponent  {
  @Input() device: Device;
  @Input() selected: boolean;
  @Output() setActive: EventEmitter<Device> = new EventEmitter<Device>();
  @Output() delete: EventEmitter<number> = new EventEmitter<number>();
  @Output() gotoDetails: EventEmitter<number> = new EventEmitter<number>();

  opened = true;

  deleteHandler(event: MouseEvent, id: number) {
    event.stopPropagation();
    this.delete.emit(id);
  }

  toggle(event: MouseEvent) {
    event.stopPropagation();
    this.opened = !this.opened
  }

  gotoDetailsHandler(event: MouseEvent, id: number) {
    event.stopPropagation();
    this.gotoDetails.emit(id);

  }
}
