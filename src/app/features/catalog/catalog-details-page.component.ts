import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Device } from '../../model/device';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-catalog-details-page',
  template: `
    <div class="alert alert-danger" *ngIf="error; else sheet">
      product not found
      <button routerLink="/catalog">GO TO PRODUCTS LIST</button>
    </div>
    
    <ng-template #sheet>
      <h1>Scheda prodotto</h1>
      <pre>{{data | json}}</pre>
      <pre>{{data?.name}}</pre>
    </ng-template>

    <hr>
    <button routerLink="/catalog/details/2">Next</button>
  `,
})
export class CatalogDetailsPageComponent implements OnInit {
  data: Device;
  error: boolean;

  constructor(private activatedRouter: ActivatedRoute, private http: HttpClient) {
    activatedRouter.params
      .pipe(
        switchMap(params => http.get<Device>('http://localhost:3000/devices/' + params.id) )
      )
      .subscribe(result => this.data = result);
  }

  ngOnInit(): void {
  }

}
