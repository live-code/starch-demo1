import { NgModule } from '@angular/core';
import { CatalogPageComponent } from './catalog-page.component';
import { CatalogComponent } from './catalog.component';
import { CatalogFormComponent } from './components/catalog-form.component';
import { CatalogListComponent } from './components/catalog-list.component';
import { CatalogFilterComponent } from './components/catalog-filter.component';
import { CatalogListItemComponent } from './components/catalog-list-item.component';
import { CatalogListItemPriceComponent } from './components/catalog-list-item-price.component';
import { CatalogListItemOsIconComponent } from './components/catalog-list-item-os-icon.component';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { FilterByNamePipe } from './pipes/filter-by-name.pipe';
import { SharedModule } from '../../shared/shared.module';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { CatalogDetailsPageComponent } from './catalog-details-page.component';

@NgModule({
  declarations: [
    // catalog feature
    CatalogPageComponent,
    CatalogComponent,
    CatalogFormComponent,
    CatalogListComponent,
    CatalogFilterComponent,
    CatalogListItemComponent,
    CatalogListItemPriceComponent,
    CatalogListItemOsIconComponent,
    FilterByNamePipe,
    CatalogDetailsPageComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    RouterModule.forChild([
      { path: 'details/:id', component: CatalogDetailsPageComponent},
      { path: '', component: CatalogPageComponent},
    ])
  ]
})
export class CatalogModule {
  
}
