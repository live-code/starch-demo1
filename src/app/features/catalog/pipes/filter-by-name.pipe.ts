import { Pipe, PipeTransform } from '@angular/core';
import { Device } from '../../../model/device';

@Pipe({
  name: 'filterByName',
})
export class FilterByNamePipe implements PipeTransform {
  transform(items: Device[], textToSearch: string = ''): unknown {
    return  items.filter(d => {
      return d.name.toLowerCase().indexOf(textToSearch.toLowerCase()) !== -1
    });
  }
}
