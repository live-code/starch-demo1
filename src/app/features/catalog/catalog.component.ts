import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Device } from '../../model/device';
import { CatalogService } from './services/catalog.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-catalog',
  template: `
    
    <div class="alert alert-danger" 
         *ngIf="catalogService.error">
      c'è une errore  
    </div>
    <app-catalog-form
      [titleName]="'Catalog'"
      [active]="catalogService.active"
      (save)="catalogService.saveHandler($event)"
      (clean)="catalogService.resetActive()"
    >
    </app-catalog-form>

    <br>
    <app-catalog-filter
      (filterByName)="catalogService.filterDevices($event)"
    ></app-catalog-filter>

    <app-catalog-list
      [devices]="catalogService.devices"
      [active]="catalogService.active"
      [text]="catalogService.filtertext"
      (setActive)="catalogService.setActive($event)"
      (delete)="catalogService.deleteHandler($event)"
      (gotoDetails)="goto($event)"
    ></app-catalog-list>
   
  `,
  providers: [
    CatalogService,
  ]
})
export class CatalogComponent {

  constructor(
    public catalogService: CatalogService,
    private router: Router
  ) {
    this.catalogService.getAll();
  }

  goto(id: number) {
    this.router.navigateByUrl('/catalog/details/' + id)
  }
}

