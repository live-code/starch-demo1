import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Device } from '../../../model/device';
import { Observable } from 'rxjs';

@Injectable()
export class CatalogService {
  devices: Device[] = [];
  active: Device;
  error: boolean;
  filtertext: string;

  constructor(private http: HttpClient) {}

  getAll() {
    this.http.get<Device[]>('http://localhost:3000/devices')
      .subscribe(res => this.devices = res);

    this.resetActive();
  }

  resetActive() {
    this.active = { os: null } as Device;
  }

  deleteHandler(id: number) {
    this.http.delete(`http://localhost:3000/devices/${id}`)
      .subscribe(
        () => {
          // const index = this.devices.findIndex(d => d.id === id);
          // this.devices.splice(index, 1);
          this.devices = this.devices.filter(d => d.id !== id)
        },
        err => {
          console.log('Catalog service error')
          this.error = true;
          setTimeout(() => this.error = false, 2000);
        }
      );
  }

  saveHandler(device: Device) {
    if (this.active && this.active.id) {
      this.edit(device);
    } else {
      this.add(device);
    }
  }

  add(device: Device) {
    this.http.post<Device>(`http://localhost:3000/devices`, device)
      .subscribe(res => {
        this.devices = [...this.devices, res]
        this.resetActive();
      });
  }

  edit(device: Device) {
    this.http.patch<Device>(`http://localhost:3000/devices/${this.active.id}`, device)
      .subscribe(res => {
        this.devices = this.devices.map(d => {
          return d.id === this.active.id ? res : d;
        });
      });
  }

  setActive(device: Device) {
    this.active = device;
  }


  filterDevices(text: string) {
    this.filtertext = text;
  }
}
