import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-catalog-page',
  template: `
    <app-row>
      <app-col>
        <app-catalog></app-catalog>
      </app-col>

      <app-col>
        <app-catalog></app-catalog>
      </app-col>
    </app-row>
  `,
  styles: [
  ]
})
export class CatalogPageComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
