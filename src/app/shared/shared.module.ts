import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TabbarComponent } from './components/tabbar.component';
import { HelloComponent } from './components/hello.component';
import { CardComponent } from './components/card.component';
import { JumbotronComponent } from './components/jumbotron.component';
import { RowComponent } from './components/row.component';
import { ColComponent } from './components/col.component';

@NgModule({
  declarations: [
    // shared
    TabbarComponent,
    HelloComponent,
    CardComponent,
    JumbotronComponent,
    RowComponent,
    ColComponent,
  ],
  imports: [
    CommonModule,
  ],
  exports: [
    TabbarComponent,
    HelloComponent,
    CardComponent,
    JumbotronComponent,
    RowComponent,
    ColComponent,
  ]
})
export class SharedModule { }
