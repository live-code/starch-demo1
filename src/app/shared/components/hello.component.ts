import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-hello',
  template: `
    <h1 [style.color]="color">hello {{label}}</h1>
  `,
  styles: [
  ]
})
export class HelloComponent {
  @Input() label = 'Guest'
  @Input() color = '#000'


}
