import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-jumbotron',
  template: `
    <div class="jumbotron">
      <ng-content></ng-content>
    </div>
  `,
  styles: [
  ]
})
export class JumbotronComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
