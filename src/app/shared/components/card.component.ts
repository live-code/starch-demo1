import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-card',
  template: `
    <div class="card mb-3">
      <div 
        *ngIf="title"
        (click)="isOpen = !isOpen "
        class="card-header"
        [ngClass]="{
          'bg-dark text-white': theme === 'dark',
          'bg-light': theme === 'light'
        }"
      >
        {{title}}
        
        <div class="pull-right">
          <i *ngIf="icon"
             [class]="icon" 
             (click)="iconClickHandler($event)"></i>
        </div>
      </div>
      
      <div class="card-body" *ngIf="isOpen">
        <ng-content></ng-content>
      </div>
    </div>
  `,
})
export class CardComponent {
  @Input() title: string;
  @Input() theme: 'dark' | 'light' = 'light';
  @Input() icon: string;
  @Input() url: string;
  @Output() iconClick: EventEmitter<void> = new EventEmitter()
  isOpen = true;

  iconClickHandler(event: MouseEvent) {
    event.stopPropagation();
    this.iconClick.emit()
  }
}
