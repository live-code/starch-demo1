import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AuthGuard } from './core/auth/auth.guard';

const routes = [
  { path: 'home', loadChildren: () => import('./features/home/home.module').then(m => m.HomeModule )},
  {
    path: 'settings',
    canActivate: [AuthGuard],
    loadChildren: () => import('./features/settings/settings.module').then(m => m.SettingsModule )},
  {
    path: 'catalog',
    canActivate: [AuthGuard],
    loadChildren: () => import('./features/catalog/catalog.module').then(m => m.CatalogModule) },
  { path: 'contacts', loadChildren: () => import('./features/contacts/contacts.module').then(m => m.ContactsModule) },
  { path: '', loadChildren: () => import('./features/login/login.module').then(m => m.LoginModule) },
  { path: '**', redirectTo: 'home'},
];


@NgModule({
  imports: [
    RouterModule.forRoot(routes),
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {

}

