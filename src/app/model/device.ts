export class Device {
  id?: number;
  name: string;
  os?: 'ios' | 'android';
  memory?: number;
  price?: number;
  date?: number;
}

